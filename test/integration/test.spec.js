const request = require('supertest')
const { expect } = require('chai')
const app = require('../../app.js')

function connectToExpress () {
    const agent = request.agent(app);
    return agent
}

describe('GET /api/items', () => {
    beforeEach(() => {

    })

    it('should return a simple JSON', async () => {
        const agent = connectToExpress();

        return agent.get('/api/items')
            .expect('Content-Type', /json/)
            .then((res) => {
                //console.log('res', res.body)
                expect(res.body)
                    .to.be.an('array')
                    .of.length(2);

                expect(res.body[0]).to.have.property('id');
                expect(res.body[0].description).to.equal("Ceci est une description");
                expect(res.body[0].id).to.equal(1);
                expect(res.body[1].id).to.equal(2);
            })

    })
})